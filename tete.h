#ifndef TETE_H_INCLUDED
#define TETE_H_INCLUDED

#define L 500
#define F 500

#endif // TETE_H_INCLUDED

void FctCrypte(FILE* fp);
int lecture(FILE* fp,char * tabSource);
void EcrirePerroquetFichier(char * perroq);
void cryptage(char* fileSource, char* perroquet,char *tabCrypte);
void decryptage(char* perroquet,char *fileSource,char *tabDeCrypte,int taille);
void supprimeFichier();
