#include <stdio.h>
#include <stdlib.h>

#include "tete.h"
#include <string.h>

int main()
{
    FILE* fp = NULL;
    fp = fopen("source.txt", "rt");
    char tabsource[L];
    int lec = lecture(fp,tabsource);
    char reponse[1];
    if(lec == 1)
    {

        char perroquet[L];
        EcrirePerroquetFichier(perroquet);
        printf("\nVoici le test que vous voulais crypte : %s \nAvec le perroquet : %s ",tabsource,perroquet);
        int tailleSource = strlen(tabsource);
        char tabCrypte[tailleSource];
        cryptage(tabsource,perroquet,tabCrypte);
        char tabDeCrypte[tailleSource];
        decryptage(perroquet,tabCrypte,tabDeCrypte,tailleSource);
        printf("\nVoulez-vous supprimer le fichier source ? O-N:");
        scanf("%s",reponse);
        if(reponse[0] == 'O' || reponse[0] == 'o')
        {
            supprimeFichier();
        }

        return 0;

    }
    printf("\nCreer le fichier source avant de lancer l'appli");
    return -1;
}
