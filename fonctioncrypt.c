#include <stdio.h>
#include <stdlib.h>

#include "tete.h"
#include <string.h>



int lecture(FILE* fp,char * tabsource)
{

    if (fp != NULL)  //Ouverture du fichier
    {
        printf("\nFichier ouvert.");

        //stocker les caracteres du fichier source dans TABLEAUSOURCE

        int i = 0;
        char l;
        fread(&l, sizeof(char), 1, fp);
        while(!feof(fp))
        {
            tabsource[i] = l;
            fread(&l, sizeof(char), 1, fp);

            if(feof(fp))
            {
                printf("\nFin de Fichier");

            }
            i++;
        }

        //Fermeture du fichier
        fclose(fp);
        return 1;

    }
    else
    {
        printf("\nEchec ouverture!!");   //Message si l'ouverture de source.txt echoue.
        return -1;
    }

    //return tabsource;
}

void EcrirePerroquetFichier(char * perroq)   //Ecrire le perroquet saisi par l'utilisateur dans le fichier perroquet.def
{
    FILE* perroquet = NULL;
    printf("\nDonner votre perroquet pour le cryptage : ");
    scanf("%s",perroq);
    perroquet = fopen("perroquet.def", "w+");    //creation, ouverture et ecriture du fichier perroquet.def
    fputs(perroq, perroquet);
    fclose(perroquet);

}

void cryptage(char* fileSource, char* perroquet,char *tabCrypte)
{
    FILE* result = NULL;
    result = fopen("destination.crt", "w+");
    int tailleSource = strlen(fileSource);
    int indicePerro = 0;
    for(int indiceSrc = 0; indiceSrc < tailleSource; indiceSrc++)
    {
        tabCrypte[indiceSrc] = fileSource[indiceSrc] - perroquet[indicePerro];
        fwrite(&tabCrypte[indiceSrc], sizeof(char), 1, result);
        //fwrite('c', sizeof(char), 1, result);
        indicePerro++;
        if(indicePerro >= strlen(perroquet))
        {
            indicePerro = 0;
        }

    }

    printf("\nLe fichier source est crypte et le resultat est dans destination.crt");

}


void decryptage(char* perroquet,char *tabCrypte,char *tabDeCrypte,int taille)
{

    //taille = taile du tableau o� est stock� fichier source
    int indicePerro = 0;
    for(int indiceSrc = 0; indiceSrc < taille; indiceSrc++)
    {
        tabDeCrypte[indiceSrc] = (tabCrypte[indiceSrc]+perroquet[indicePerro]);
        indicePerro++;
        if(indicePerro >= strlen(perroquet))
        {
            indicePerro = 0;
        }

    }
    printf("\nVoici le resultat du decryptage : %s",tabDeCrypte);
    tabDeCrypte[taille] ='\0';


}


void supprimeFichier()  //pour supprimer le fichier source selon l'utilisateur
{

    if (remove("source.txt") == 0)
    {
        printf("\nLe fichier %s a ete supprime avec succes.");
    }
    else
    {
        printf("\nImpossible de supprimer le fichier");

    }

}


